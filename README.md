[Sublime Text Bootstrap Jade](https://bitbucket.org/Tumbo/bootstrap-jade)
========================================

Sublime Text 2 Twitter Bootstrap snippets with Jade output. Direct conversion from [DEVtellect's Bootstrap package](http://github.com/devtellect/sublime-twitter-bootstrap-snippets/).

## Installation
*  Clone the repository into your packages folder.

	git clone https://Tumbo@bitbucket.org/Tumbo/bootstrap-jade.git
 

## License
License: MIT [http://www.opensource.org/licenses/mit-license.php](http://www.opensource.org/licenses/mit-license.php)

